
const FIRST_NAME = "Radu";
const LAST_NAME = "Tofan";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }
    getDetails(){
        return String(this.name+' '+this.surname+' '+this.salary);
    }
}

class SoftwareEngineer extends Employee {
    constructor(name,surname,salary,experience ){
        super(name,surname,salary);
        if(experience){
            this.experience=experience;
        }
        else{
            this.experience='JUNIOR';
        }

    }
    applyBonus(){
        if(this.experience==='JUNIOR'){
            return this.salary*1.1;
        }
        else if(this.experience==='MIDDLE'){
            return this.salary*1.15;
        }
        else if(this.experience==='SENIOR')
        {
            return this.salary*1.2;
        }
        else{
            return this.salary*1.1;
        }
        
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

